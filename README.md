<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.users

An [Ansible](https://ansible.com) role that allows for adding (non-existing) or removing (existing) users on a system and for adding SSH public keys to the [authorized_keys](https://man.archlinux.org/man/core/openssh/ssh.1.en#FILES) file of users.

## Requirements

An Arch Linux system or an Arch Linux chroot.

## Role Variables

By default this role doesn't do anything, as `users` is an empty list.

* `ssh_authorized_keys`: a list of dicts 
  * `authorized_keys`: a list of authorized keys to add for a given user
  * `name`: the username for which to add authorized keys (`_all` has special meaning, in that the keys in question are added for all users)

* `users`: a list containing data about users to create or remove (`[]` by default). **NOTE**: As this list contains user passwords, it should be defined in an [ansible-vault](https://man.archlinux.org/man/ansible-vault.1) encrypted file!
  * `name`: a string defining the name of the user
  * `group`: a string defining the default group of the user (the group must exist!). This is not required when `name` is `root`.
  * `comment`: a string defining a comment about the user. This is not required when `name` is `root`.
  * `groups`: a list of strings defining ofany supplementary groups (the groups must exist!). This is not required when `name` is `root`.
  * `state`: a string defining the state of existence of the user (`present` or `absent`). This is not required when `name` is `root`.
  * `password`: a string defining the plaintext password for the user
  * `shell`: a string defining the shell for the user (must match an entry in `users_config.allowed_shells`)
  * `authorized_keys`: a list of [SSH public keys](https://man.archlinux.org/man/ssh.1#AUTHENTICATION) with which one can authenticate a session for the user
  * `git_repos`: a list of dicts, that defines git repositories to clone for the specific user. **NOTE**: In a chroot the `dest` directories are always pruned (as if `force` was set to `true`)
    * `dest`: a string defining a relative directory (may not start with `/` and may not contain `..`) into which to clone the repository. **NOTE**: Using the string `.` defines a repository directly in the user's home. In this case no skeleton files are copied when the user home is created (to minimize the possibility for file conflicts).
    * `force`: a boolean value indicating whether to remove an existing `dest` directory before cloning. **NOTE**: This is ignored if `dest` is `.`!
    * `repo`: a string defining the repository URL to clone from. **NOTE**: Only upstream URLs that do not require authentication are supported!
    * `version`: a string defining the version of the repository to checkout

* `users_config`: a dict providing settings that apply to the entire role, but not necessarily directly to any user in particular (defaults to `{}`)
  * `no_log`: whether to hide log output when working with user data (defaults to `true`). **NOTE**: Setting this to `false` will leak user passwords into the log!
  * `user_home`: a string defining where user homes are located (defaults to `/home`)
  * `allowed_shells`: a list of shells that can be set for users (defaults to `["/usr/bin/bash", "/usr/bin/zsh"]`)

The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)

## Dependencies

None

## Example Playbooks

```yaml
- name: Add a user
  hosts: my_hosts
  vars:
    users:
      - name: user_name
        group: some_group
        comment: 'Comment about user_name'
        groups:
          more_group
          other_group
        state: present
        password: 'some password from vault'
        git_repos:
          - dest: foo
            repo: some_upstream_url
            version: HEAD
            force: false
  tasks:
    - name: Include archlinux.users
      ansible.builtin.include_role:
        name: archlinux.users
```

```yaml
- name: Remove a user
  hosts: my_hosts
  vars:
    users:
      - name: user_name
        comment: 'Comment about why user_name should be removed'
        state: absent
  tasks:
    - name: Include archlinux.users
      ansible.builtin.include_role:
        name: archlinux.users
```

```yaml
- name: Add SSH public keys to ~/.ssh/authorized_keys
  hosts: my_hosts
  vars:
    users:
      - name: user_name
        group: some_group
        comment: 'Comment about user_name'
        groups: 
          - more_group
          - other_group
        state: present
        password: 'some password from vault'
        authorized_keys:
          - 'some public key'
          - 'some other public key'
    ssh_authorized_keys:
      - name: user_name
        authorized_keys:
          - 'some key this user should have on all systems'
          - 'some other key'
  tasks:
    - name: Include archlinux.users
      ansible.builtin.include_role:
        name: archlinux.users
```

```yaml
- name: Add SSH public key for all users
  hosts: my_hosts
  vars:
    ssh_authorized_keys:
      - name: _all
        authorized_keys:
          - 'this key will be set for all users on the system'
  tasks:
    - name: Include archlinux.users
      ansible.builtin.include_role:
        name: archlinux.users
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
