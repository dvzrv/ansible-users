# SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
# SPDX-License-Identifier: GPL-3.0-or-later
---

- name: Verify
  hosts: all
  gather_facts: false
  vars:
    required_authorized_keys_user:
      - fake_all
      - fake_foo_global
      - fake_foo
    required_authorized_keys_root:
      - fake_all
    git_repo_dirs:
      - /home/foo/.git
      - /home/foo/devtools/.git
  tasks:
    - name: Get file information on authorized_keys file for user foo
      ansible.builtin.stat:
        path: /home/foo/.ssh/authorized_keys
      register: authorized_keys_user_file

    - name: Fail if authorized_keys file for user foo does not exist
      ansible.builtin.fail:
        msg: /home/foo/.ssh/authorized_keys has not been created!
      when: not authorized_keys_user_file.stat.exists

    - name: Read authorized keys file of user
      ansible.builtin.command: "cat /home/foo/.ssh/authorized_keys"
      register: authorized_keys_user
      changed_when: false

    - name: Show authorized_keys_file
      ansible.builtin.debug:
        var: authorized_keys_user

    - name: Fail if authorized_keys file does not contain keys
      ansible.builtin.fail:
        msg: "/home/foo/.ssh/authorized_keys does not contain {{ item }}"
      loop: "{{ required_authorized_keys_user }}"
      when: not item in authorized_keys_user.stdout_lines

    - name: Get file information on git repositories for user foo
      ansible.builtin.stat:
        path: "{{ item }}"
      register: git_repo_stats
      loop: "{{ git_repo_dirs }}"

    - name: Fail if a git repo dir for user foo does not exist
      ansible.builtin.fail:
        msg: "{{ item }} has not been created!"
      when: not item.stat.exists
      loop: "{{ git_repo_stats.results }}"

    - name: Get file information on .bashrc for foo user
      ansible.builtin.stat:
        path: /home/foo/.bashrc
      register: foo_bashrc

    - name: Fail if .bashrc exists
      ansible.builtin.fail:
        msg: "The .bashrc should not exist!"
      when: foo_bashrc.stat.exists

    - name: Get file information on authorized_keys file for root
      ansible.builtin.stat:
        path: /root/.ssh/authorized_keys
      register: authorized_keys_root_file

    - name: Fail if authorized_keys file for root does not exist
      ansible.builtin.fail:
        msg: /root/.ssh/authorized_keys has not been created!
      when: not authorized_keys_root_file.stat.exists

    - name: Read authorized keys file of user
      ansible.builtin.command: "cat /root/.ssh/authorized_keys"
      register: authorized_keys_root
      become: true
      become_user: root
      changed_when: false

    - name: Show authorized_keys_file
      ansible.builtin.debug:
        var: authorized_keys_root

    - name: Fail if authorized_keys file does not contain keys
      ansible.builtin.fail:
        msg: "/root/.ssh/authorized_keys does not contain {{ item }}"
      loop: "{{ required_authorized_keys_root }}"
      when: not item in authorized_keys_root.stdout_lines
